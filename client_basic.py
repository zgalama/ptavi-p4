#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""
import socket
import sys


def main():

    server = sys.argv[1]
    port = int(sys.argv[2])
    line = ' '.join(sys.argv[3:])

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.sendto(line.encode('utf-8'), (server, port))
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))

    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
