#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socketserver
import sys
import time
import json


class SIPRequest:

    def __init__(self, data):
        self.data = data

    def parse(self):
        received = self.data.decode('utf-8')
        self._parse_command(received)
        self._parse_headers(received.index('\n'))

    def _get_address(self, uri):
        try:
            schema = uri.split(':')[0]
            address = uri.split(':')[1]
            return address, schema
        except:
            schema = None
            address = None
            return address, schema

    def _parse_command(self, line):
        self.command = (line.split(' ')[0]).upper()
        self.uri = line.split(' ')[1]
        elementsURI = self._get_address(self.uri)
        address = elementsURI[0]
        schema = elementsURI[1]
        self.address = address
        if self.command == 'REGISTER':
            if schema != 'sip':
                self.result = '416 Unsupported URI Scheme'
            else:
                self.result = '200 OK'
        else:
            self.result = '405 Method Not Allowed'

    def _parse_headers(self, first_nl):
        self.headers = {}
        received = self.data.decode('utf-8')
        userHeaders = received[first_nl:]
        for line in userHeaders.splitlines():
            if line != '':
                head = line.split(':')[0].replace(' ', '')
                value = line.split(':')[1].replace(' ', '')
                self.headers[head] = value


class SIPRegisterHandler(socketserver.BaseRequestHandler):
    usersDict = {}
    usersHeaders = {}

    def process_register(self, sip_request):
        self.usersDict[sip_request.address] = f'{self.client_address[0]}'
        for header, value in sip_request.headers.items():
            if (header == 'Expires') and (value == '0'):
                self.usersDict.pop(sip_request.address)

        print(self.usersDict)

    def registered2json(self, sip_request):
        self.usersHeaders[sip_request.address] = sip_request.headers
        expires_time_format = time.strftime('%Y-%m_%d %H:%M:%S', time.gmtime(time.time()))
        user_info = []

        for address, header in self.usersHeaders.items():
            expires_time = f'{expires_time_format} +{header["Expires"]}'
            user_info.append({address: {'address': self.client_address[0], 'expires': expires_time}})
        json_write = json.dumps(user_info, indent=1)

        with open('registered.json', 'w') as file:
            file.write(str(json_write))
        file.close()

    def json2registered(self):
        with open('registered.json', 'r') as file:
            data = json.loads(file.read())
        file.close()
        for user in data:
            for key, value in user.items():
                self.usersDict[key] = value['address']
        pass

    def handle(self):
        data = self.request[0]
        sock = self.request[1]
        sip_request = SIPRequest(data)
        sip_request.parse()
        if (sip_request.command == "REGISTER") and (sip_request.result == "200 OK"):
            self.process_register(sip_request)
            self.registered2json(sip_request)
        sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(), self.client_address)


def main():
    PORT = int(sys.argv[1])

    try:
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
        print(f"Server listening in port {PORT}")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()

